package com.getjavajob.training.web1706.test;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Phone;
import com.getjavajob.training.web1706.common.Request;
import com.getjavajob.training.web1706.dao.AccountDao;
import com.getjavajob.training.web1706.dao.PhoneDao;
import com.getjavajob.training.web1706.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;


@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    private AccountDao accountDao;
    private PhoneDao phoneDao;
    private Account account;
    private Account friend;
    private AccountService accountService;

    @Before
    public void init() throws ParseException {
        account = new Account("Ivanov", "Ivan", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("11.11.2011"), "ivanov@mail.ru", "2344455", 'm',
                "home address", "work address",
                "@telegram","about me");
        friend = new Account( "Petrov", "Ivan", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("11.11.2011"), "petrov@mail.ru", "2344455", 'm',
                "home address", "work address",
                "@telegram","about me");

        accountDao = mock(AccountDao.class);
        phoneDao = mock(PhoneDao.class);
        account.setId(1);
        friend.setId(2);
        accountService = new AccountService(accountDao, phoneDao);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAccountTest() {
        Account mock = mock(Account.class);
        when(accountDao.getById(mock.getId())).thenReturn(mock);

        assertEquals(mock, accountService.getAccount(new Account().getId()));
        assertNotEquals(mock(Account.class), accountService.getAccount(new Account().getId()));

        verify(accountDao, times(2)).getById(new Account().getId());
    }

    @Test
    public void getAllAccountsTest() {
        List<Account> list = new ArrayList<>();
        list.add(mock(Account.class));
        list.add(mock(Account.class));
        when(accountDao.getAll()).thenReturn(list);

        assertEquals(list, accountService.getAllAccounts());

        verify(accountDao, times(1)).getAll();
    }

    @Test
    public void createAccountTest() {
        when(accountDao.insert(account)).thenReturn(account);
        assertEquals(accountService.createAccount(account), account);
    }

    @Test
    public void removeAccountTest() {
        Account mock = mock(Account.class);
        accountService.deleteAccount(mock);
        verify(accountDao).delete(mock);
    }

    @Test
    public void updateAccountTest() {
        when(accountDao.getById(account.getId())).thenReturn(account);
        accountService.updateAccount(account);
        verify(accountDao).update(account);
    }

    @Test
    public void sendRequestToFriendTest() throws ParseException {
        Account mockAcc = mock(Account.class);
        Account mockFriend = mock(Account.class);

        when(accountDao.getById(account.getId())).thenReturn(mockAcc);
        when(accountDao.getById(friend.getId())).thenReturn(mockFriend);

        accountService.sendRequestToFriend(account, friend);
        verify(mockAcc).addRequestsFromMe(mockFriend);
    }

    @Test
    public void deleteFriendTest() throws ParseException {
        Account mockAcc = mock(Account.class);
        Account mockFriend = mock(Account.class);

        when(accountDao.getById(account.getId())).thenReturn(mockAcc);
        when(accountDao.getById(friend.getId())).thenReturn(mockFriend);

        accountService.sendRequestToFriend(account, friend);
        verify(mockAcc).addRequestsFromMe(mockFriend);
        accountService.deleteRequest(account, friend);
        verify(mockAcc).removeRequestsFromMe(mockFriend);
    }

    @Test
    public void acceptFriendTest() {
        when(accountDao.getById(account.getId())).thenReturn(account);
        when(accountDao.getById(friend.getId())).thenReturn(friend);

        accountService.acceptFriend(account, friend);
        verify(accountDao).acceptFriend(account, friend);
    }

    @Test
    public void rejectFriendTest() {
        when(accountDao.getById(account.getId())).thenReturn(account);
        when(accountDao.getById(friend.getId())).thenReturn(friend);

        accountService.rejectFriend(account, friend);
        verify(accountDao).rejectFriend(account, friend);
    }

    @Test
    public void getAllFriendsTest() {
        Account mockAcc = mock(Account.class);
        Account mockFriend = mock(Account.class);

        Request request = new Request(account, friend);
        List<Request> requests = new ArrayList<>();
        requests.add(request);
        List<Account> friends = new ArrayList<>();
        friends.add(friend);

        when(accountDao.getById(account.getId())).thenReturn(mockAcc);
        when(accountDao.getById(friend.getId())).thenReturn(mockFriend);
        accountService.sendRequestToFriend(account, friend);

        when(accountDao.getFriends(any(Account.class))).thenReturn(requests);
        accountService.getFriends(account);
        verify(accountDao).getFriends(account);
        assertEquals(friends, accountService.getFriends(account));
    }

    @Test
    public void getAllFollowersTest() {
        Account mockAcc = mock(Account.class);
        Account mockFriend = mock(Account.class);

        Request request = new Request(friend, account);
        List<Request> requests = new ArrayList<>();
        requests.add(request);
        List<Account> followers = new ArrayList<>();
        followers.add(friend);

        when(accountDao.getById(account.getId())).thenReturn(mockAcc);
        when(accountDao.getById(friend.getId())).thenReturn(mockFriend);
        accountService.sendRequestToFriend(account, friend);

        when(accountDao.getFollowers(any(Account.class))).thenReturn(requests);
        accountService.getFollowers(account);
        verify(accountDao).getFollowers(account);
        assertEquals(followers, accountService.getFollowers(account));
    }

    @Test
    public void getRecipientsTest() {
        Account mockAcc = mock(Account.class);
        Account mockFriend = mock(Account.class);

        Request request = new Request(account, friend);
        List<Request> requests = new ArrayList<>();
        requests.add(request);
        List<Account> recipients = new ArrayList<>();
        recipients.add(friend);

        when(accountDao.getById(account.getId())).thenReturn(mockAcc);
        when(accountDao.getById(friend.getId())).thenReturn(mockFriend);
        accountService.sendRequestToFriend(account, friend);

        when(accountDao.getRecipients(any(Account.class))).thenReturn(requests);
        accountService.getRecipients(account);
        verify(accountDao).getRecipients(account);
        assertEquals(recipients, accountService.getRecipients(account));
    }

    @Test
    public void addPhoneTest() {
        Phone mock = mock(Phone.class);
        when(phoneDao.insert(any(Phone.class))).thenReturn(mock);

        assertEquals(mock, accountService.addPhone(new Phone()));
        verify(phoneDao, times(1)).insert(any(Phone.class));
    }

    @Test
    public void getAllPhonesTest() {
        Phone phone = new Phone("99999", account);
        List<Phone> phones = new ArrayList<>();
        phones.add(phone);
        when(phoneDao.getAllPhonesByIdAccount(any(Integer.class))).thenReturn(phones);
        accountService.addPhone(phone);
        verify(phoneDao, times(1)).insert(any(Phone.class));
        assertEquals(phones, accountService.getPhones(account));
    }

    @Test
    public void searchAccountsForPaginationTest() {
        String expr = "ov";
        int start = 2;
        int maxResult = 3;
        List<Account> list = new ArrayList<>();
        when(accountDao.searchAccountsForPagination(expr, start, maxResult)).thenReturn(list);

        List<Account> result = accountService.searchAccountsForPagination(expr, start, maxResult);
        verify(accountDao).searchAccountsForPagination(expr, start, maxResult);
        assertEquals(list, result);
    }

    @Test
    public void getCountAccountsForPaginationTest() {
        String expr = "ov";
        int count = 2;
        when(accountDao.getCountAccountsForPagination(expr)).thenReturn(count);

        int result = accountService.getCountAccountsForPagination(expr);
        verify(accountDao).getCountAccountsForPagination(expr);
        assertEquals(count, result);
    }
}
