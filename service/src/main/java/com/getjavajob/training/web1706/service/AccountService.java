package com.getjavajob.training.web1706.service;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Relation;
import com.getjavajob.training.web1706.common.Request;
import com.getjavajob.training.web1706.common.Phone;
import com.getjavajob.training.web1706.dao.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service("accountService")
public class AccountService implements GenericService {

    @Autowired
    private AccountDao dao;
    @Autowired
    private PhoneDao phoneDao;

    public AccountService() {
    }

    public AccountService(AccountDao dao, PhoneDao phoneDao) {
        this.dao = dao;
        this.phoneDao = phoneDao;
    }

    public Account getAccount(int id) {
        Account account = dao.getById(id);
        downloadPhones(account);
        return account;
    }

    public List<Account> getAllAccounts() {
        return dao.getAll();
    }

    public Account getByEmail(String email) {
        Account account = new Account();
        if (email != null) {
            account = dao.getByEmail(email);
            downloadPhones(account);
        }
        return account;
    }

    public byte[] getAvatarById(int id) {
        return dao.getAvatarById(id);
    }

    @Transactional
    public Account createAccount(Account account) {
        if (account != null) {
            return dao.insert(account);
        }
        return null;
    }

    @Transactional
    public Account updateAccount(Account account) {
        if (account != null) {
            Account accountDB = dao.getById(account.getId());
            account.setRequestsFrom(accountDB.getRequestsFrom());
            account.setRequestsTo(accountDB.getRequestsTo());
            return dao.update(account);
        }
        return null;
    }

    @Transactional
    public void deleteAccount(Account account) {
        if (account != null) {
            dao.delete(account);
        }
    }

    @Transactional
    public void sendRequestToFriend(Account account, Account friend) {
        if (account != null && friend != null) {
            Account fromAccount = dao.getById(account.getId());
            Account toAccount = dao.getById(friend.getId());
            fromAccount.addRequestsFromMe(toAccount);
        }
    }

    @Transactional
    public void deleteRequest(Account account, Account friend) {
        if (friend != null) {
            Account fromAccount = dao.getById(account.getId());
            Account toAccount = dao.getById(friend.getId());
            fromAccount.removeRequestsFromMe(toAccount);
        }
    }

    @Transactional
    public void deleteFriend(Account account, Account friend) {
        if (friend != null) {
            dao.deleteFriend(account, friend);
        }
    }

    @Transactional
    public void acceptFriend(Account account, Account friend) {
        if (account != null && friend != null) {
            Account fromAccount = dao.getById(account.getId());
            Account toAccount = dao.getById(friend.getId());
            dao.acceptFriend(fromAccount, toAccount);
        }
    }

    @Transactional
    public void rejectFriend(Account account, Account friend) {
        if (account != null && friend != null) {
            Account fromAccount = dao.getById(account.getId());
            Account toAccount = dao.getById(friend.getId());
            dao.rejectFriend(fromAccount, toAccount);
        }
    }

    public List<Account> getFriends(Account account) {
        List<Account> friends = new ArrayList<>();
        if (account != null) {
            List<Request> requests = dao.getFriends(account);
            if (requests != null) {
                for (Request request : requests) {
                    if (request.getAccount().equals(account)) {
                        friends.add(request.getFriend());
                    } else {
                        friends.add(request.getAccount());
                    }
                }
            }
        }
        return friends;
    }

    public int getCountFriends(Account account) {
        return getFriends(account).size();
    }

    public List<Account> getFollowers(Account account) {
        List<Account> followers = new ArrayList<>();
        if (account != null) {
            List<Request> requests = dao.getFollowers(account);
            if (requests != null) {
                for (Request request : requests) {
                    followers.add(request.getAccount());
                }
            }
        }
        return followers;
    }

    public int getCountFollowers(Account account) {
        return getFollowers(account).size();
    }

    public List<Account> getRecipients(Account account) {
        List<Account> recipients = new ArrayList<>();
        if (account != null) {
            List<Request> requests = dao.getRecipients(account);
            if (requests != null) {
                for (Request request : requests) {
                    recipients.add(request.getFriend());
                }
            }
        }
        return recipients;
    }

    public int getCountRecipients(Account account) {
        return getRecipients(account).size();
    }

    @Transactional
    public Phone addPhone(Phone phone) {
        Phone result = null;
        if (phone != null) {
            result = phoneDao.insert(phone);
        }
        return result;
    }

    public List<Phone> getPhones(Account account) {
        List<Phone> phones = new ArrayList<>();
        if (account != null) {
            phones = phoneDao.getAllPhonesByIdAccount(account.getId());
        }
        return phones;
    }

    public void removePhone(Phone phone) {
        phoneDao.delete(phone);
    }

    public List<Account> searchAccounts(String expr) {
        return dao.searchAccounts(expr);
    }

    public List<Account> searchAccountsForPagination(String expr, int start, int maxResult) {
        return dao.searchAccountsForPagination(expr, start, maxResult);
    }

    public int getCountAccountsForPagination(String expr) {
        return dao.getCountAccountsForPagination(expr);
    }

    public Relation getAccountRelation(int idFrom, int idTo) {
        Account from = dao.getById(idFrom);
        Account to = dao.getById(idTo);

        Request sentRequest = dao.getRequest(from, to);
        Request request = sentRequest != null ? sentRequest : dao.getRequest(to, from);

        if (idFrom == idTo) {
            return Relation.MY_ACCOUNT;
        } else if (request == null) {
            return Relation.GUEST;
        } else if (request.getAccount().equals(from) && !request.isAccepted()) {
            return Relation.RECIPIENT;
        } else if (request.getFriend().equals(from) && !request.isAccepted()) {
            return Relation.FOLLOWER;
        } else if (request.isAccepted()) {
            return Relation.FRIEND;
        }

        return Relation.MY_ACCOUNT;
    }

    private void downloadPhones(Account account) {
        if (account != null) {
            List<Phone> phones = phoneDao.getAllPhonesByIdAccount(account.getId());
            if (phones != null) {
                account.setPhones(phones);
            }
        }
    }
}
