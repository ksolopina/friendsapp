# My Social Network "Friends"  

**Functionality:** 

* registration
* authentication
* ajax search with pagination
* ajax autocomplete search
* display profile
* edit profile
* upload and download avatar
* users export/upload to/from xml
* friendship/membership relations   

**Tools:** JDK 8,
Spring 4,  
JPA 2 / Hibernate 5, 
jQuery 3, 
Bootstrap 3, 
JUnit 4, 
Mockito, 
Maven, 
Git / Bitbucket, 
Tomcat 8, 
MySQL, 
IntelliJIDEA 2017.

**Notes:**
SQL ddl is located in the dao/src/main/resources/db.sql

**Screenshots:**

![Welcome page](screenshots/welcome.png)
![Validation](screenshots/validation.png)
![Home page](screenshots/account.png)
![Search ajax](screenshots/search-ajax.png)
![Edit page](screenshots/edit.png)
![Search page](screenshots/search.png)
![Friends page](screenshots/friends.png)

_  
**Solopina Kseniya**  
Training getJavaJob  
[http://www.getjavajob.com](http://www.getjavajob.com)