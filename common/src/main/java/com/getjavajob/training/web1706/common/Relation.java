package com.getjavajob.training.web1706.common;

public enum Relation {
    FRIEND, FOLLOWER, RECIPIENT, GUEST, MY_ACCOUNT
}
