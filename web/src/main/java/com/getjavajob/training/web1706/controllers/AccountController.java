package com.getjavajob.training.web1706.controllers;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Phone;
import com.getjavajob.training.web1706.common.Relation;
import com.getjavajob.training.web1706.service.GenericService;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.getjavajob.training.web1706.utils.Utils.getEncodedFile;

@Controller
@SessionAttributes("accountSession")
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private GenericService service;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @RequestMapping(value = "/account/{accId}", method = RequestMethod.GET)
    public ModelAndView getAccountPage(@ModelAttribute("accountSession") Account accountSession, @PathVariable int accId, HttpSession session) throws ServletException {
        logger.info("page accountId = " + accId);
        if (accountSession == null) {
            return new ModelAndView("welcome");
        }
        ModelAndView modelAndView = new ModelAndView("account");
        logger.info("account from session " + accountSession);
        Account account = service.getAccount(accId);
        logger.info("account from page " + account);
        Relation relation =  service.getAccountRelation(accountSession.getId(), accId);
        modelAndView.addObject("relation", relation.name());
        modelAndView.addObject("account", account);
        String encodedFile = getEncodedFile(service.getAvatarById(accId));
        if (encodedFile != null) {
            modelAndView.addObject("avatar", encodedFile);
            logger.info("avatar was added");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView updatePage(@ModelAttribute("accountSession") Account account) {
        logger.info("update page for accountId = " + account.getId());
        account = service.getAccount(account.getId());
        ModelAndView modelAndView = new ModelAndView("update");
        modelAndView.addObject("account", account);
        return modelAndView;
    }

    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, headers = {"content-type=multipart/form-data"})
    public String doUpdate(@ModelAttribute("accountSession") Account accountSession, HttpSession session,
                           @ModelAttribute("account") Account account) throws ServletException, IOException {
        account.setId(accountSession.getId());
        account.setPassword(accountSession.getPassword());

        byte[] avatar = account.getAvatar();
        if (avatar == null || avatar.length == 0) {
            account.setAvatar(service.getAvatarById(accountSession.getId()));
        }

        removeNullPhones(account);
        preparePhone(account);

        service.updateAccount(account);

        logger.info("succes update for " + account.getId());

        avatar = account.getAvatar();
        if (avatar != null) {
            String encodedfile = new String(Base64.encodeBase64(avatar), "UTF-8");
            session.setAttribute("avatar", encodedfile);
        }
        session.setAttribute("accountSession", account);
        return "redirect:/account/" + account.getId();
    }

    private void preparePhone(Account account) {
        List<Phone> newPhones = account.getPhones();
        if (newPhones != null) {
            for (Phone phone : newPhones) {
                phone.setAccount(account);
            }
        } else {
            account.setPhones(service.getPhones(account));
            account.getPhones().clear();
        }
    }

    private Account removeNullPhones(Account account) {
        if (account.getPhones() != null) {
           account.getPhones().removeIf(phone -> phone.getPhone() == null);
        }
        return account;
    }

}