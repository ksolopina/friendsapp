package com.getjavajob.training.web1706.utils;

import com.getjavajob.training.web1706.common.Phone;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public class SHA256 {

    public static void main(String[] args) {
        System.out.println(getSHA256Hash("123").equals(getSHA256Hash("123")));
        System.out.println(getSHA256Hash("aniston"));
        System.out.println(new Phone());
    }

    public static String getSHA256Hash(String data) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes("UTF-8"));
            return bytesToHex(hash);
        }catch(Exception ex) {
                ex.printStackTrace();
        }
        return null;
    }

    private static String bytesToHex(byte[] hash) {
        return DatatypeConverter.printHexBinary(hash);
    }
}
