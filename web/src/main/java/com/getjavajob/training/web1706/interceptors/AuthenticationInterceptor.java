package com.getjavajob.training.web1706.interceptors;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.controllers.AccountController;
import com.getjavajob.training.web1706.service.GenericService;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private GenericService service;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getSession().getAttribute("accountSession") == null) {
            if (hasCookies(request)) {
                if (!isLogged(request)) {
                    response.sendRedirect(request.getContextPath() + "/welcome");
                    return false;
                }
            } else {
                response.sendRedirect(request.getContextPath() + "/welcome");
                return false;
            }
        }
        return super.preHandle(request, response, handler);
    }

    private boolean isLogged(HttpServletRequest request) throws UnsupportedEncodingException {
        Cookie cookie = getCookie(request);
        if (cookie != null) {
            Account account = service.getByEmail(cookie.getValue());
            if (account != null) {
                byte[] avatar = account.getAvatar();
                if (avatar != null) {
                    String encodedfile = new String(Base64.encodeBase64(avatar), "UTF-8");
                    request.getSession().setAttribute("avatar", encodedfile);
                }
                request.getSession().setAttribute("accountSession", account);
                return true;
            }
        }
        return false;
    }

    private boolean hasCookies(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie ck : cookies) {
                if ("emailCookie".equals(ck.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    private Cookie getCookie(HttpServletRequest request) {
        if (request.getCookies() != null) {
            for (Cookie ck : request.getCookies()) {
                if ("emailCookie".equals(ck.getName())) {
                    return ck;
                }
            }
        }
        return null;
    }
}
