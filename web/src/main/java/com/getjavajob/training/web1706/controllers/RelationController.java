package com.getjavajob.training.web1706.controllers;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.DTO.AccountDTO;
import com.getjavajob.training.web1706.service.GenericService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import static com.getjavajob.training.web1706.utils.Utils.getListAccountDTO;

@Controller
@SessionAttributes("accountSession")
public class RelationController {
    private static final Logger logger = LoggerFactory.getLogger(RelationController.class);

    @Autowired
    private GenericService service;

    @RequestMapping(value = "/sendFriendRequest")
    public ModelAndView sendFriendRequest(@ModelAttribute("accountSession") Account accountSession,
                                          @RequestParam("accId") int accId) {
        Account account = service.getAccount(accountSession.getId());
        Account friend = service.getAccount(accId);
        service.sendRequestToFriend(account, friend);
        logger.info("request from account " + accountSession.getId() + " to account "  + accId);
        return new ModelAndView("redirect:/account/" + accId);
    }

    @RequestMapping(value = "/acceptFriendRequest")
    public ModelAndView acceptFriendRequest(@ModelAttribute("accountSession") Account accountSession,
                                            @RequestParam("accId") int accId, HttpServletRequest request) {
        String referer = request.getHeader("Referer");
        Account account = service.getAccount(accountSession.getId());
        Account friend = service.getAccount(accId);
        service.acceptFriend(account, friend);
        service.getAccountRelation(account.getId(), friend.getId());
        logger.info("account " + accountSession.getId() + " accepted account " + accId);
        return new ModelAndView("redirect:" + referer);
    }

    @RequestMapping(value = "/deleteFriend")
    public ModelAndView deleteFriend(@ModelAttribute("accountSession") Account accountSession,
                                     @RequestParam("accId") int accId, HttpServletRequest request) throws MalformedURLException {
        String referer = request.getHeader("Referer");
        Account account = service.getAccount(accountSession.getId());
        Account friend = service.getAccount(accId);
        service.deleteFriend(account, friend);
        logger.info("account " + accountSession.getId() + " deleted account " + accId);
        return new ModelAndView("redirect:" + referer);
    }

    @RequestMapping(value = "/showFriends")
    public ModelAndView showFriends(@RequestParam("accId") int accId) throws ServletException {
        Account account = service.getAccount(accId);
        List<Account> accounts = service.getFriends(account);
        return showAccounts(accounts, account, "FRIEND");
    }

    @RequestMapping(value = "/showFollowers")
    public ModelAndView showFollowers(@RequestParam("accId") int accId) throws ServletException {
        Account account = service.getAccount(accId);
        List<Account> accounts = service.getFollowers(account);
        return showAccounts(accounts, account, "FOLLOWER");
    }

    @RequestMapping(value = "/showRecipients")
    public ModelAndView showRecipients(@RequestParam("accId") int accId) throws ServletException {
        Account account = service.getAccount(accId);
        List<Account> accounts = service.getRecipients(account);
        return showAccounts(accounts, account, "RECIPIENT");
    }

    private ModelAndView showAccounts(List<Account> accounts, Account account, String relation) throws ServletException {
        ModelAndView modelAndView = new ModelAndView("/friends");
        List<AccountDTO> allAccounts = getListAccountDTO(accounts);
        modelAndView.addObject("accounts", allAccounts);
        modelAndView.addObject("account", account);
        modelAndView.addObject("relation", relation);
        return modelAndView;
    }

}
