package com.getjavajob.training.web1706.controllers;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.service.GenericService;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static com.getjavajob.training.web1706.utils.SHA256.getSHA256Hash;

@Controller
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private GenericService service;

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String getWelcomePage(HttpSession session) {
        Account accountSession = (Account) session.getAttribute("accountSession");
        return accountSession == null ? "welcome" : "redirect:/account/" + accountSession.getId() ;
    }

    @RequestMapping(value = "/logIn", method = RequestMethod.POST)
    public ModelAndView logIn(@RequestParam("email") String email, @RequestParam("password") String pass,
                              @RequestParam(value = "remember", required = false) String remember,
                              HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        logger.info("login " + email);
        Account account = service.getByEmail(email);
        String hash = getSHA256Hash(pass);
        if (account != null && account.getPassword().equals(hash)) {
            if (remember != null) {
                Cookie emailCookie = new Cookie("emailCookie", email);
                Cookie passwordCookie = new Cookie("passwordCookie", hash);
                resp.addCookie(emailCookie);
                resp.addCookie(passwordCookie);
            }
            byte[] avatar = account.getAvatar();
            if (avatar != null) {
                String encodedfile;
                try {
                    encodedfile = new String(Base64.encodeBase64(avatar), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    throw new ServletException(e);
                }
                req.getSession().setAttribute("avatar", encodedfile);
            }
            req.getSession().setAttribute("accountSession", account);
            logger.info("succes log in " + email);
            return new ModelAndView("redirect:/account/" + account.getId());
        } else {
            req.setAttribute("message", "Неверный логин или пароль");
            logger.warn("wrong login or password " + email);
            return new ModelAndView("welcome");
        }

    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registration(@ModelAttribute("account") Account account, HttpSession session) {
        account.setPassword(getSHA256Hash(account.getPassword()));
        Account acc = service.createAccount(account);
        session.setAttribute("accountSession", acc);
        logger.info("succes registration");
        return new ModelAndView("redirect:/account/"+ acc.getId());
    }

    @RequestMapping(value = "/logOut", method = RequestMethod.GET)
    public String logOut(HttpSession session, HttpServletResponse resp) {
        Cookie emailCookie = new Cookie("emailCookie",null);
        Cookie passwordCookie = new Cookie("passwordCookie",null);
        emailCookie.setMaxAge(0);
        passwordCookie.setMaxAge(0);
        resp.addCookie(emailCookie);
        resp.addCookie(passwordCookie);
        session.invalidate();
        logger.info("log out");
        return "welcome";
    }
}
