package com.getjavajob.training.web1706.controllers;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.DTO.AccountDTO;
import com.getjavajob.training.web1706.common.Phone;
import com.getjavajob.training.web1706.service.GenericService;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import javax.servlet.ServletException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.getjavajob.training.web1706.utils.Utils.getEncodedFile;
import static com.getjavajob.training.web1706.utils.Utils.getListAccountDTO;
import static java.lang.Math.ceil;

@Controller
public class SearchController {
    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    private static final int COUNT_ACCOUNTS_PER_PAGE = 2;

    @Autowired
    private GenericService service;

    @RequestMapping(value = "/autocomplete", method = RequestMethod.GET)
    @ResponseBody
    public List<AccountDTO> getSearchResult(@RequestParam("search") String filter) throws ServletException {
        return getListAccountDTO(service.searchAccounts(filter));
    }

    @RequestMapping(value = "/search")
    public ModelAndView search(@RequestParam("search") String filter) throws ServletException {
        logger.info("search accounts");
        double countAccounts = service.getCountAccountsForPagination(filter);
        ModelAndView modelAndView = new ModelAndView("/searchResult");
        List<Account> accounts = service.searchAccountsForPagination(filter, 0, COUNT_ACCOUNTS_PER_PAGE);
        List<AccountDTO> searchAccounts = getListAccountDTO(accounts);
        modelAndView.addObject("searchAccounts", searchAccounts);
        modelAndView.addObject("countAccountsPerPage", COUNT_ACCOUNTS_PER_PAGE);
        int count = (int) ceil(countAccounts / COUNT_ACCOUNTS_PER_PAGE);
        modelAndView.addObject("countPages", count);
        modelAndView.addObject("search", filter);
        return modelAndView;
    }

    @RequestMapping(value = "/showNextPage")
    @ResponseBody
    public List<AccountDTO> getNextPage(@RequestParam("search") String filter,
                                        @RequestParam("start") int start, @RequestParam("maxResult") int maxResult) throws ServletException {
        List<Account> accounts = service.searchAccountsForPagination(filter, start, maxResult);
        return getListAccountDTO(accounts);
    }

}
