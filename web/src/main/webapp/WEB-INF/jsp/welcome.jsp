<%@ include file="common/headerWelcome.jsp" %>
<div class="welcome-content">
    <div class="row welcome-row">
        <div class="col-md-5 col-sm-5">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Войдите на сайт</h3>
                        <p>Введите логин и пароль:</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-key"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <div class="help-block errors">${message}</div>
                    <form role="form" action="/logIn" method="post" class="login-form">
                        <div class="form-group">
                            <label class="sr-only" for="login-form-username">Email</label>
                            <input name="email" placeholder="email" class="login-form-username form-control" id="login-form-username" type="email"
                                   required>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="login-form-password">Password</label>
                            <input type="password" name="password" placeholder="Пароль" class="login-form-password form-control" id="login-form-password"
                                   data-error="Минимум 5 символов" required>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" id="remember"> Запомнить меня
                            </label>
                        </div>
                        <button type="submit" class="btn">Войти!</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2">
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Зерегистрируйтесь сейчас</h3>
                        <p>Заполните форму:</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-pencil"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="/registration" method="post" class="registration-form">
                        <div class="form-group">
                            <label class="sr-only" for="form-first-name">First name</label>
                            <input type="text" name="firstName" placeholder="Имя" class="form-first-name form-control" id="form-first-name"
                                   required>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-last-name">Last name</label>
                            <input type="text" name="lastName" placeholder="Фамилия" class="form-last-name form-control" id="form-last-name"
                                   required>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-email">Email</label>
                            <input type="email" name="email" placeholder="email" class="form-email form-control" id="form-email"
                                   data-error="Некорректный email" required>
                            <div class="help-block  with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-password">Password</label>
                            <input type="password" name="password" placeholder="Пароль..." class="form-password form-control" id="form-password"
                                   data-minlength="5" data-error="Минимум 5 символов" required>
                            <div class="help-block  with-errors"></div>
                        </div>
                        <button type="submit" class="btn">Зарегистрироваться!</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="common/footer.jsp" %>
