<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv = "Content-Language" content = "ru"/>
    <meta http-equiv = "Content-Type" content="text/html; charset=utf-8">
    <title>FriendsApp</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" type="text/css"/>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" type="text/css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/form-elements.css" type="text/css"/>
</head>
<body>
    <div class="wrapper">
        <header class="header">
            <div class="inner-container">
                <div class="container">
                    <div class="row header-content">
                        <div class="col-md-3 logo-title">
                            <a href="${pageContext.request.contextPath}/account/${sessionScope.accountSession.id}">
                                <i class="fa fa-users logo" aria-hidden="true"></i>
                                <div class="logo-title-inner">
                                    <div class="main-title">Friends</div>
                                    <div class="sub-title"> Keep in touch</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-8 col-sm-9 col-xs-12">
                                <form class="navbar-form" role="search" method="post" action="/search">
                                    <div class="form-group ui-widget">
                                        <input id="search" type="search" class="form-control" name="search">
                                        <i class="fa fa-search icon-zoom" aria-hidden="true"></i>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4 col-sm-3 col-xs-12 logout">
                                <a href="/logOut"><i class="fa fa-sign-out" aria-hidden="true"></i>Выйти</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="content">
            <div class="container">
