<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions"%>
<%@ include file="common/header.jsp" %>
    <div class="row">
        <div class="tab-content no-border padding-24">
            <div id="home" class="tab-pane in active">
                <div class="col-md-4 col-sm-12 col-xs-12  center">
                    <div class="profile-picture avatar-big">
                        <c:choose>
                            <c:when test="${account.avatar != null}">
                                <img class="editable img-responsive avatar" src="data:image/jpeg;base64,${avatar}"/>
                            </c:when>
                            <c:otherwise>
                                <img src="${pageContext.request.contextPath}/img/avatar.png"
                                     class="editable img-responsive avatar">
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="wrapper-group-btn">
                        <div class="group-btn">
                            <c:if test='${relation != "MY_ACCOUNT"}'>
                                <a href="${pageContext.request.contextPath}/account/${sessionScope.accountSession.id}"
                                   class="btn btn-sm btn-friend">
                                    <span class="bigger-110">Вернуться на свою страницу</span>
                                </a>
                            </c:if>
                            <c:if test='${relation == "GUEST"}'>
                                <a href="${pageContext.request.contextPath}/sendFriendRequest?accId=${account.id}"
                                   class="btn btn-sm btn-friend">
                                    <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                                    <span class="bigger-110">Отправить запрос</span>
                                </a>
                            </c:if>
                            <c:if test='${relation == "FOLLOWER"}'>
                                <a href="${pageContext.request.contextPath}/acceptFriendRequest?accId=${account.id}"
                                   class="btn btn-sm btn-friend">
                                    <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                                    <span class="bigger-110">Добавить в друзья</span>
                                </a>
                                <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                                   class="btn btn-sm btn-friend">
                                    <i class="ace-icon fa fa-minus-circle bigger-120"></i>
                                    <span class="bigger-110">Удалить запрос</span>
                                </a>
                            </c:if>
                            <c:if test='${relation == "RECIPIENT"}'>
                                <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                                   class="btn btn-sm btn-friend">
                                    <i class="ace-icon fa fa-minus-circle bigger-120"></i>
                                    <span class="bigger-110">Удалить запрос</span>
                                </a>
                            </c:if>
                            <c:if test='${relation == "FRIEND"}'>
                                <a type="button" class="btn btn-sm btn-friend disabled">Вы друзья</a>
                                <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                                   class="btn btn-sm btn-friend">
                                    <i class="ace-icon fa fa-minus-circle bigger-120"></i>
                                    <span class="bigger-110">Удалить из друзей</span>
                                </a>
                            </c:if>
                            <c:if test='${relation == "MY_ACCOUNT"}'>
                                <a href="${pageContext.request.contextPath}/showFriends?accId=${account.id}"
                                   class="btn btn-sm btn-friend">
                                    <i class="ace-icon fa fa-user bigger-120"></i>
                                    <span class="bigger-110">Показать друзей</span>
                                </a>
                                <a href="${pageContext.request.contextPath}/update"
                                   class="btn btn-sm btn-friend">
                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                    <span class="bigger-110">Редактировать</span>
                                </a>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="user-info">
                        <h3 class="user-name">
                            <c:out value="${account.firstName}"/>
                            <c:out value="${account.middleName}"/>
                            <c:out value="${account.lastName}"/>
                        </h3>
                        <div class="profile-user-info">
                            <c:if test='${account.birthDate != null || not empty account.info}'>
                                <div class="profile-info-group">
                                    <c:if test='${account.birthDate != null}'>
                                        <div class="profile-info-row">
                                        <div class="profile-info-title">Дата рождения:</div>
                                        <div>
                                            <fmt:formatDate value="${account.birthDate}" pattern="dd.MM.yyyy" var="newdatevar" />
                                            <c:out value="${newdatevar}" />
                                        </div>
                                    </div>
                                    </c:if>
                                    <c:if test='${not empty account.info}'>
                                        <div class="profile-info-row information-block">
                                            <div class="profile-info-title">О себе:</div>
                                            <div class="about-me">
                                                <c:out value="${account.info}"/>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </c:if>
                            <div class="profile-info-group">
                                <div class="profile-info-row">
                                    <div class="contact-info-title">Контактная информация</div>
                                </div>
                                <c:if test='${not empty account.phones}'>
                                    <div class="profile-info-row">
                                        <div class="profile-info-title">Телефон:</div>
                                        <div class="wrapper-phones">
                                            <c:forEach items="${account.phones}" var="phone">
                                                <div class="phone">
                                                    <c:out value="${phone.phone}"/>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </c:if>
                                <div class="profile-info-row">
                                    <div class="profile-info-title">Email:</div>
                                    <div>
                                        <c:out value="${account.email}"/>
                                    </div>
                                </div>
                                <c:if test='${not empty account.skype}'>
                                    <div class="profile-info-row">
                                        <div class="profile-info-title">Skype:</div>
                                        <div>
                                            <c:out value="${account.skype}"/>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test='${not empty account.telegram}'>
                                    <div class="profile-info-row">
                                        <div class="profile-info-title">Telegram:</div>
                                        <div>
                                            <c:out value="${account.telegram}"/>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test='${not empty account.homeAddress}'>
                                    <div class="profile-info-row">
                                        <div class="profile-info-title">Домашний адрес:</div>
                                        <div>
                                            <c:out value="${account.homeAddress}"/>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test='${not empty account.workAddress}'>
                                    <div class="profile-info-row">
                                        <div class="profile-info-title">Рабочий адрес:</div>
                                        <div>
                                            <c:out value="${account.workAddress}"/>
                                        </div>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<%@ include file="common/footer.jsp" %>
