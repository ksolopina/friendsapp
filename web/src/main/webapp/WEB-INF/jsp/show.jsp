<%@ include file="common/header.jsp" %>
<div class="row">
    <div class="tab-content no-border padding-24">
        <div id="home" class="tab-pane in active">
            <div class="col-xs-12 col-sm-4 center">
                <div class="profile-picture avatar-big">
                    <c:choose>
                        <c:when test="${avatarShow != null}">
                            <img class="editable img-responsive avatar" src="data:image/jpeg;base64,${avatarShow}"/>
                        </c:when>
                        <c:otherwise>
                            <img src="${pageContext.servletContext.contextPath}/img/avatar.png"
                                 class="editable img-responsive avatar">
                        </c:otherwise>
                    </c:choose>
                </div>
                <a href="#" class="btn btn-sm btn-friend">
                    <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                    <span class="bigger-110">Добавить в друзья</span>
                </a>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="user-info">
                    <h4 class="user-name">
                        <c:out value="${accountShow.firstName}"/>
                        <c:out value="${accountShow.middleName}"/>
                        <c:out value="${accountShow.lastName}"/>
                    </h4>
                    <div class="profile-user-info">
                        <div class="profile-info-row">
                            <div class="profile-info-title">Дата рождения:</div>
                            <div>
                                <i class="fa fa-birthday-cake bigger-110"></i>
                                <c:out value="${accountShow.birthDate}"/>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-title">Дата регистрации:</div>
                            <div></div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-title">Пол:</div>
                            <div></div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-title">О себе:</div>
                            <div>
                                <p><c:out value="${accountShow.info}"/></p>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="contact-info-title">Контактная информация</div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-title">Телефон:</div>
                            <c:forEach items="${accountShow.phones}" var="phone">
                                <div class="phone">
                                    <i class="fa fa-phone bigger-110"></i>
                                    <c:out value="${phone.phone}"/>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-title">Email:</div>
                            <div>
                                <i class="fa fa-envelope-o bigger-110"></i>
                                <c:out value="${accountShow.email}"/>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-title">Skype:</div>
                            <div>
                                <i class="fa fa-skype bigger-110"></i>
                                <c:out value="${accountShow.skype}"/>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-title">Telegram:</div>
                            <div>
                                <i class="fa fa-telegram bigger-110"></i>
                                <c:out value="${accountShow.telegram}"/>
                            </div>
                        </div>

                        <div class="profile-info-row">
                            <div class="profile-info-title">Домашний адрес:</div>
                            <div>
                                <i class="fa fa-map-marker bigger-110"></i>
                                <c:out value="${accountShow.homeAddress}"/>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-title">Рабочий адрес:</div>
                            <div><i class="fa fa-map-marker bigger-110"></i>
                                <c:out value="${accountShow.workAddress}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="common/footer.jsp" %>