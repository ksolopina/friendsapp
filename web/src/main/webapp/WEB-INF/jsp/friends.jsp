<%@ include file="common/header.jsp" %>
<nav>
    <div id="navbarNavAltMarkup">
        <div class="nav-friends" id="nav-friends">
            <a class="arrow-link" href="${pageContext.request.contextPath}/account/${account.id}">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
            </a>
            <a class="nav-item nav-link"
               href="${pageContext.request.contextPath}/showFriends?accId=${account.id}">
                Друзья
            </a>
            <a class="nav-item nav-link"
               href="${pageContext.request.contextPath}/showFollowers?accId=${account.id}">
                Подписчики
            </a>
            <a class="nav-item nav-link"
               href="${pageContext.request.contextPath}/showRecipients?accId=${account.id}">
                Отправленные запросы
            </a>
        </div>
    </div>
</nav>

<div class="friends-wrapper">
    <c:set var="index" value="0"/>
    <c:forEach items="${accounts}" var="account">
        <div class="friends-block" id="blockSearch${index}">
            <div class="profile-picture avatar-small">
                <c:choose>
                    <c:when test="${account.avatar != null}">
                        <img id="avatar${index}" class="editable img-responsive avatar" src="data:image/jpeg;base64,${account.avatar}"/>
                    </c:when>
                    <c:otherwise>
                        <img id="avatar${index}" src="${pageContext.request.contextPath}/img/avatar.png"
                             class="editable img-responsive avatar">
                    </c:otherwise>
                </c:choose>
            </div>
            <div>
                <a href="${pageContext.request.contextPath}/account/${account.id}" id="link${index}">${account.firstName} ${account.lastName}</a>
            </div>
            <c:if test='${relation == "FOLLOWER"}'>
                <a href="${pageContext.request.contextPath}/acceptFriendRequest?accId=${account.id}"
                   class="btn btn-sm btn-friend">
                    <i class="ace-icon fa fa-plus-circle bigger-120"></i>
                    <span class="bigger-110">Добавить в друзья</span>
                </a>
                <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                   class="btn btn-sm btn-friend">
                    <i class="ace-icon fa fa-minus-circle bigger-120"></i>
                    <span class="bigger-110">Удалить запрос</span>
                </a>
            </c:if>
            <c:if test='${relation == "RECIPIENT"}'>
                <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                   class="btn btn-sm btn-friend">
                    <i class="ace-icon fa fa-minus-circle bigger-120"></i>
                    <span class="bigger-110">Удалить запрос</span>
                </a>
            </c:if>
            <c:if test='${relation == "FRIEND"}'>
                <a href="${pageContext.request.contextPath}/deleteFriend?accId=${account.id}"
                   class="btn btn-sm btn-friend">
                    <i class="ace-icon fa fa-minus-circle bigger-120"></i>
                    <span class="bigger-110">Удалить из друзей</span>
                </a>
            </c:if>
        </div>
        <c:set var="index" value="${index + 1}"/>
    </c:forEach>
</div>
<%@ include file="common/footer.jsp" %>