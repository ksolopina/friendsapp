$('form').validator({

    // the delay in milliseoncds
    delay: 500,

    // allows html inside the error messages
    html: false,

    // disable submit button if there's invalid form
    disable: true,

    // <a href="https://www.jqueryscript.net/tags.php?/Scroll/">Scroll</a> to and focus the first field with an error upon validation of the form.
    focus: true,

    // define your custom validation rules
    custom: {},

    // default errof messages
    errors: {
        minlength: 'Not long enough'
    },

    // feedback CSS classes
    feedback: {
        success: 'glyphicon-ok',
        error: 'glyphicon-remove'
    }

})