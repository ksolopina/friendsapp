$(document).ready(function(){
    $(function () {
        upload();
    });

    function upload() {
        var input = $('#uploadXML');
        input.change(function (e) {
            var files = e.target.files;
            var reader = new FileReader();
            reader.readAsText(files[0]);
            reader.onload = function() {
                var xmlData = reader.result;
                if (window.DOMParser) {
                    parser = new DOMParser();
                    xmlDoc = parser.parseFromString(xmlData,"text/xml");
                } else {
                    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async = false;
                    xmlDoc.loadXML(xmlData);
                }
                parseXml(xmlDoc);
            };
        });
    }
    
    function parseXml(xmlDoc) {
        saveNodeValue($('#form-first-name'), "firstName");
        saveNodeValue($('#form-last-name'), "lastName");
        saveNodeValue($('#form-middle-name'), "middleName");
        savePhonesNodeValue();
        saveNodeValue($('#form-home-address'), "homeAddress");
        saveNodeValue($('#form-work-address'), "workAddress");
        saveNodeValue($('#form-email'), "email");
        saveNodeValue($('#form-telegram'), "telegram");
        saveNodeValue($('#form-skype'), "skype");
        saveNodeValue($('#form-info'), "info");
        saveNodeValue($('#date'), "dateBirth");
    }

    function saveNodeValue(tagName, nodeName) {
        tagName.val(xmlDoc.getElementsByTagName(nodeName)[0].childNodes[0].nodeValue);
    }
    
    function savePhonesNodeValue() {
        var phones = xmlDoc.getElementsByTagName("phones")[0].childNodes;
        var countPhonesPerPage = $('#phones').children(".phone").length;
        var count = 0;
        for(var i = 0; i < phones.length; i++) {
            var phone = phones[i].textContent;
            if (phone.trim().length !== 0) {
                if (count >= countPhonesPerPage) {
                    addRowForPhone();
                }
                $('#phone' + count).val(phone);
                count++;
            }
        }
    }

    function addRowForPhone() {
        var phones = $('.update-form').find('#phones');
        var phoneCount = getNumbers($('.input-phone').last().attr('name'));
        var btnAdd = phones.find('.wrap-btn');
        var account = phones.find('#account-id').val();
        var newIn = '<div class="phone"><input type="text" name="phones['+ ++phoneCount +'].phone" ' +
            'id="phone' + phoneCount + '" class="form-phone form-control input-phone" ' +
            'placeholder="+7 (999) 999-9999" data-toggle="popover" data-placement="left" ' +
            'data-content="Поле телефона не может быть пустым">' +
            '<button class="btn btn-danger remove new-btn">-</button></div>';
        btnAdd.before(newIn);
    }

    function getNumbers(inputString){
        var regex=/\d+\.\d+|\.\d+|\d+/g,
            results = [],
            n;
        while(n = regex.exec(inputString)) {
            results.push(parseFloat(n[0]));
        }
        return results;
    }

});