$(document).ready(function(){
    var xmlFile = null;

    $(function () {
        download();
    });

    function download() {
        var input = $('#downloadXMLlink');
        input.click(function () {
            var xml = prepareXml();
            var link = $('#downloadXMLlink');
            link.attr("href", makeXml(xml));
        });
    }

    function makeXml(xml) {
        var data = new Blob([xml], {type: 'plain/xml'});
        if (xmlFile !== null) {
            window.URL.revokeObjectURL(xmlFile);
        }
        xmlFile = window.URL.createObjectURL(data);
        return xmlFile;
    }

    function prepareXml() {
        var xml = [];
        xml.push("\<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        xml.push("\<account\>");
        xml.push("\<firstName>" + $('#form-first-name').val() + "\</firstName>");
        xml.push("\<lastName>" + $('#form-last-name').val() + "\</lastName>");
        xml.push("\<middleName>" + $('#form-middle-name').val() + " \</middleName>");
        xml.push("\<phones>");
        var phones = $("#phones").find($('div.phone input'));
        for (var i = 0; i < phones.length ; i++) {
            xml.push("\<phone>" + phones[i].value + "\</phone>");
        }
        xml.push("\</phones>");
        xml.push("\<homeAddress>" + $('#form-home-address').val() + "\</homeAddress>");
        xml.push("\<workAddress>" + $('#form-work-address').val() + "\</workAddress>");
        xml.push("\<email>" + $('#form-email').val() + "\</email>");
        xml.push("\<telegram>" + $('#form-telegram').val() + "\</telegram>");
        xml.push("\<skype>" + $('#form-skype').val() + "\</skype>");
        xml.push("\<info>" + $('#form-info').val() +"\</info>");
        xml.push("\<dateBirth>" + $('#date').val() + "\</dateBirth>");
        xml.push("\</account>");
        return xml.join('\n');
    }

});