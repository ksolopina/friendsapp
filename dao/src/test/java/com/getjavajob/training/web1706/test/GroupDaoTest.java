package com.getjavajob.training.web1706.test;

import com.getjavajob.training.web1706.common.Group;
import com.getjavajob.training.web1706.dao.GroupDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class GroupDaoTest {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private GroupDao groupDao;
    private Group group;
    private List<Group> groups;
    private ResourceDatabasePopulator databasePopulator;

    @Before
    public void init() throws SQLException, IOException, ClassNotFoundException {
        Resource createTables = new ClassPathResource("queriesCreate.sql");
        databasePopulator = new ResourceDatabasePopulator(createTables);
        databasePopulator.execute(dataSource);
        group = new Group("Group", "about group");
        group = groupDao.insert(group);
        groups = new ArrayList<>();
        groups.add(group);
    }

    @After
    public void terminate() throws SQLException {
        Resource dropTables = new ClassPathResource("queriesDrop.sql");
        databasePopulator.setScripts(dropTables);
        databasePopulator.execute(dataSource);
    }

    @Test
    public void getAllTest() {
        List<Group> groupsDao = groupDao.getAll();
        int i = 0;
        assertEquals(groups.size(), groupsDao.size());
        for (Group group : groups) {
            assertEquals(group, groupsDao.get(i));
            i++;
        }
    }

    @Test
    public void getByIdTest() {
        assertEquals(group, groupDao.getById(group.getId()));
    }

    @Test
    public void deleteByIdTest() {
        groupDao.delete(group);
        assertNull(groupDao.getById(23));
    }

    @Test
    public void updateByIdTest() {
        group.setInfo("New info");
        groupDao.update(group);
        assertEquals(group, groupDao.getById(group.getId()));
    }

    @Test
    public void insertTest() {
        Group newGroup = new Group( "new group", "new group");
        newGroup = groupDao.insert(newGroup);
        assertEquals(newGroup, groupDao.getById(newGroup.getId()));
    }
}
