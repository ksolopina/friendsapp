package com.getjavajob.training.web1706.test;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Request;
import com.getjavajob.training.web1706.dao.AccountDao;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import javax.swing.text.html.parser.Entity;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class AccountDaoTest {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private AccountDao accountDao;
    private Account account;
    private List<Account> accounts;
    private Account friend;
    private List<Account> friends;
    private ResourceDatabasePopulator databasePopulator;

    @Before
    public void init() throws ParseException, SQLException, IOException, ClassNotFoundException {
        Resource createTables = new ClassPathResource("queriesCreate.sql");
        databasePopulator = new ResourceDatabasePopulator(createTables);
        databasePopulator.execute(dataSource);

        account = new Account("Ivanov", "Ivan", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("11.11.2011"), "ivanov@mail.ru",
                "12345", 'm',"home address", "work address",
                "@telegram","about me");
        friend = new Account("Petrov", "Petr", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("10.05.1990"), "petrov@mail.ru", "12344",
                'm',"home address", "work address",
                "@petro","about me");
        accounts = new ArrayList<>();
        friends = new ArrayList<>();
        friends.add(friend);
        account = accountDao.insert(account);
        friend = accountDao.insert(friend);
        accounts.add(account);
        accounts.add(friend);
    }

    @After
    public void terminate() throws SQLException {
        Resource dropTables = new ClassPathResource("queriesDrop.sql");
        databasePopulator.setScripts(dropTables);
        databasePopulator.execute(dataSource);
    }

    @Test
    public void getByIdTest() {
        assertEquals(account, accountDao.getById(1));
    }

    @Test
    public void getAllTest() {
        List<Account> accountsDao = accountDao.getAll();
        int i = 0;
        assertEquals(accounts.size(), accountsDao.size());
        for (Account account : accounts) {
            assertEquals(account, accountsDao.get(i));
            i++;
        }
    }

    @Test
    public void getByEmailTest() {
        assertEquals(account, accountDao.getByEmail("ivanov@mail.ru"));
    }

    @Transactional
    @Test
    public void deleteTest() {
        accountDao.delete(account);
        assertNull(accountDao.getById(account.getId()));
    }

    @Test
    public void updateTest() {
        account.setHomeAddress("New home address");
        accountDao.update(account);
        assertEquals(account, accountDao.getById(1));
    }

    @Test
    public void insertTest() throws ParseException {
        Account newAccount = new Account("Petrov", "Petr", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("10.05.1990"), "petrov123@mail.ru", "123445",
                'm',"home address", "work address",
                "@petro","about me");
        newAccount = accountDao.insert(newAccount);
        assertEquals(newAccount, accountDao.getById(3));
    }

    @Test
    public void searchAccountsTest() {
        List<Account> result = accountDao.searchAccounts("Pet");
        assertEquals(result.get(0), friend);
    }

    @Test
    public void searchAccountsForPaginationTest() throws ParseException {
        Account newAccount1 = new Account();
        Account newAccount2 = new Account();
        newAccount1.setLastName("Petrov");
        newAccount1.setFirstName("Serg");
        newAccount1.setEmail("petrovich@mail.com");
        newAccount1.setPassword("123");
        newAccount2.setLastName("Krasinov");
        newAccount2.setFirstName("Serg");
        newAccount2.setEmail("serg@mail.com");
        newAccount2.setPassword("123");

        newAccount1 = accountDao.insert(newAccount1);
        newAccount2 = accountDao.insert(newAccount2);

        List<Account> result = accountDao.searchAccountsForPagination("ov", 2, 3);
        assertEquals(2, result.size());
        result = accountDao.searchAccountsForPagination("ov", 0, 3);
        assertEquals(3, result.size());
    }

    @Test
    public void getCountAccountsForPaginationTest() {
        Account newAccount1 = new Account();
        Account newAccount2 = new Account();
        newAccount1.setLastName("Petrov");
        newAccount1.setFirstName("Serg");
        newAccount1.setEmail("petrovich@mail.com");
        newAccount1.setPassword("123");
        newAccount2.setLastName("Krasinov");
        newAccount2.setFirstName("Serg");
        newAccount2.setEmail("serg@mail.com");
        newAccount2.setPassword("123");

        newAccount1 = accountDao.insert(newAccount1);
        newAccount2 = accountDao.insert(newAccount2);

        int result = accountDao.getCountAccountsForPagination("ov");
        assertEquals(4, result);
    }

    @Test
    public void getAvatarById() {
        byte[] array = new BigInteger("1111000011110001", 2).toByteArray();
        account.setAvatar(array);
        accountDao.update(account);
        assertArrayEquals(account.getAvatar(), accountDao.getAvatarById(account.getId()));
    }

    @Test
    public void getFollowersTest() throws ParseException {
        account.addRequestsToMe(friend);
        assertEquals(1, accountDao.getFollowers(account).size());
        accountDao.acceptFriend(account, friend);
        assertEquals(0, accountDao.getFollowers(account).size());
    }

    @Test
    public void getRecipientsTest() {
        account.addRequestsFromMe(friend);
        assertEquals(1, accountDao.getRecipients(account).size());
        accountDao.acceptFriend(friend, account);
        assertEquals(0, accountDao.getRecipients(account).size());
    }

    @Test
    public void getFriendsTest() {
        account.addRequestsToMe(friend);
        assertEquals(0, accountDao.getFriends(account).size());
        accountDao.acceptFriend(account, friend);
        assertEquals(1, accountDao.getFriends(account).size());
    }

    @Test
    public void acceptFriendTest() {
        account.addRequestsFromMe(friend);
        accountDao.acceptFriend(friend, account);
        assertTrue(accountDao.getRequest(account, friend).isAccepted());
    }

    @Test
    public void rejectFriendTest() {
        account.addRequestsFromMe(friend);
        accountDao.acceptFriend(friend, account);
        accountDao.rejectFriend(friend, account);
        assertFalse(accountDao.getRequest(account, friend).isAccepted());
    }

    @Test
    public void removeRequestsFromMeTest() {
        account.addRequestsFromMe(friend);
        assertNotNull(accountDao.getRequest(account, friend));
        account.removeRequestsFromMe(friend);
        assertNull(accountDao.getRequest(account, friend));
    }


    @Test
    public void removeRequestsToMeTest() {
        account.addRequestsToMe(friend);
        assertNotNull(accountDao.getRequest(friend, account));
        account.removeRequestsToMe(friend);
        assertNull(accountDao.getRequest(friend, account));
    }

    @Test
    public void addRequestFromMeTest()  {
        account.addRequestsFromMe(friend);
        Request request = accountDao.getById(account.getId()).getRequestsFrom().get(0);
        assertEquals(request.getFriend(), friend);
    }

    @Test
    public void addRequestToMeTest()  {
        account.addRequestsToMe(friend);
        Request request = accountDao.getById(account.getId()).getRequestsTo().get(0);
        assertEquals(request.getAccount(), friend);
    }
}
