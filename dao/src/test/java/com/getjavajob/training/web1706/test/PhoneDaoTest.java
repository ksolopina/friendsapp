package com.getjavajob.training.web1706.test;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Phone;
import com.getjavajob.training.web1706.dao.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
public class PhoneDaoTest {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private PhoneDao phoneDao;
    @Autowired
    AccountDao accountDao;
    private Phone phone;
    private List<Phone> phones;
    private Account account;
    private ResourceDatabasePopulator databasePopulator;

    @Before
    public void init() throws SQLException, ParseException, IOException, ClassNotFoundException {
        Resource createTables = new ClassPathResource("queriesCreate.sql");
        databasePopulator = new ResourceDatabasePopulator(createTables);
        databasePopulator.execute(dataSource);

        account = new Account("Ivanov", "Ivan", "Ivanovich",
                new SimpleDateFormat("dd.MM.yyyy").parse("11.11.2011"), "ivanov@mail.ru", "1233", 'm',
                "home address", "work address",
                "@telegram","about me");
        account = accountDao.insert(account);
        phone = new Phone("90100011122", account);
        phones = new ArrayList<>();
        phones.add(phone);
        phone = phoneDao.insert(phone);
    }

    @After
    public void terminate() throws SQLException {
        Resource dropTables = new ClassPathResource("queriesDrop.sql");
        databasePopulator.setScripts(dropTables);
        databasePopulator.execute(dataSource);
    }

    @Test
    public void getAllTest() {
        List<Phone> groupsDao = phoneDao.getAll();
        int i = 0;
        assertEquals(phones.size(), groupsDao.size());
        for (Phone phone : phones) {
            assertEquals(phone, groupsDao.get(i));
            i++;
        }
    }

    @Test
    public void getByIdTest() {
        assertEquals(phone, phoneDao.getById(phone.getId()));
    }

    @Test
    public void deleteByIdTest() {
        phoneDao.delete(phone);
        assertNull(phoneDao.getById(phone.getId()));
    }

    @Test
    public void updateByIdTest() {
        phone.setPhone("9080000000");
        phoneDao.update(phone);
        assertEquals(phone, phoneDao.getById(phone.getId()));
    }

    @Test
    public void insertTest() {
        Phone newPhone = new Phone("9030000000", account);
        newPhone = phoneDao.insert(newPhone);
        assertEquals(newPhone, phoneDao.getById(newPhone.getId()));
    }

    @Test
    public void getAllPhonesByIdAccountTest() {
        Phone newPhone = new Phone("9030000000", account);
        newPhone = phoneDao.insert(newPhone);
        assertEquals(2, phoneDao.getAllPhonesByIdAccount(account.getId()).size());
        assertTrue(phoneDao.getAllPhonesByIdAccount(account.getId()).contains(phone) &&
                phoneDao.getAllPhonesByIdAccount(account.getId()).contains(newPhone));

    }

    @Test
    public void deleteAllPhonesByIdAccountTest() {
        Phone newPhone = new Phone("9030000000", account);
        newPhone = phoneDao.insert(newPhone);
        assertEquals(2, phoneDao.getAllPhonesByIdAccount(account.getId()).size());
        phoneDao.deleteAllPhonesByIdAccount(account.getId());
        assertEquals(0, phoneDao.getAllPhonesByIdAccount(account.getId()).size());
    }
}
