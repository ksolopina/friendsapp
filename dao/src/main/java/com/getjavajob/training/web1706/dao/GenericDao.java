package com.getjavajob.training.web1706.dao;

import com.getjavajob.training.web1706.dao.utils.DAOException;
import java.util.List;

public interface GenericDao<E> {

    List<E> getAll() throws DAOException;

    E getById(int id);

    void delete(E obj);

    E update(E obj);

    E insert(E obj);

}
