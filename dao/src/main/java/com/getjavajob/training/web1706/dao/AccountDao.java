package com.getjavajob.training.web1706.dao;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Request;
import com.getjavajob.training.web1706.dao.utils.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static java.lang.String.valueOf;

@Repository("accountDao")
public class AccountDao extends AbstractDao<Account> {

    public AccountDao() {
    }

    @Override
    protected CriteriaQuery<Account> getEntityQuery(CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.createQuery(Account.class);
    }

    @Override
    protected Root<Account> getEntityRoot(CriteriaQuery<?> criteriaQuery) {
        return criteriaQuery.from(Account.class);
    }

    @Override
    protected Class<Account> getEntityClass() {
        return Account.class;
    }

    public Account getByEmail(String email) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> cq = criteriaBuilder.createQuery(Account.class);
        Root<Account> from = cq.from(Account.class);
        cq.select(from).where(criteriaBuilder.equal(from.get("email"), email));
        Account account;
        try {
           account = entityManager.createQuery(cq).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return account;
    }

    public List<Account> searchAccounts(String expr) {
        return getSearchQuery(expr).getResultList();
    }

    public List<Account> searchAccountsForPagination(String expr, int start, int maxResult) {
        return getSearchQuery(expr).setFirstResult(start).setMaxResults(maxResult).getResultList();
    }

    private TypedQuery<Account> getSearchQuery(String expr) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> from = cq.from(Account.class);
        Expression<String> exp1 = cb.concat(from.get("firstName"), " ");
        exp1 = cb.concat(exp1, from.get("lastName"));
        Expression<String> exp2 = cb.concat(from.get("lastName"), " ");
        exp2 = cb.concat(exp2, from.get("firstName"));
        Predicate whereClause = cb.or(cb.like(exp1, "%"+ expr +"%"), cb.like(exp2, "%"+ expr +"%"));
        cq.select(from).where(whereClause);
        return entityManager.createQuery(cq);
    }

    public int getCountAccountsForPagination(String expr) {
        List<Account> accounts = searchAccounts(expr);
        System.out.println(accounts);
        return searchAccounts(expr).size();
    }

    public byte[] getAvatarById(int id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<byte[]> cq = criteriaBuilder.createQuery(byte[].class);
        Root<Account> from = cq.from(Account.class);
        cq.select(from.get("avatar")).where(criteriaBuilder.equal(from.get("id"), id));
        TypedQuery<byte[]> q = entityManager.createQuery(cq);
        return q.getSingleResult();
    }

    public Request getRequest(Account account, Account friend) {
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Request> cq = cb.createQuery(Request.class);
            Root<Request> from = cq.from(Request.class);
            cq.select(from).where (
                cb.and (
                        cb.equal(from.get("account"), account.getId()),
                        cb.equal(from.get("friend"), friend.getId())
                )
            );
            return entityManager.createQuery(cq).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Request> getFollowers(Account account) {
        return getRequests(account, "friend", false);
    }

    public List<Request> getRecipients(Account account) {
        return getRequests(account, "account", false);
    }

    public List<Request> getRequests(Account account, String fieldName, boolean accepted) {
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Request> cq = cb.createQuery(Request.class);
            Root<Request> from = cq.from(Request.class);
            cq.select(from).where(
                    cb.and(cb.equal(from.get(fieldName), account.getId()),
                    cb.equal(from.get("accepted"), accepted))
            );
            return entityManager.createQuery(cq).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Request> getFriends(Account account) {
        List<Request> friends = new ArrayList<>();
        List<Request> listFriendsOne = getRequests(account, "account", true);
        List<Request> listFriendsTwo = getRequests(account, "friend", true);

        if (!listFriendsOne.isEmpty()) {
            friends.addAll(listFriendsOne);
        }

        if (!listFriendsTwo.isEmpty()) {
            friends.addAll(listFriendsTwo);
        }
        return friends;
    }

    public void acceptFriend(Account account, Account friend) {
        getRequest(friend, account).setAccepted(true);
    }

    public void rejectFriend(Account account, Account friend) {
        getRequest(friend, account).setAccepted(false);
    }

    public void deleteFriend(Account account, Account friend) {
        Request sendReq = getRequest(account, friend);
        Request request =  sendReq != null ? sendReq : getRequest(friend, account);
        entityManager.remove(request);
    }
}
