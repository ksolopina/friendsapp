package com.getjavajob.training.web1706.dao;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.dao.utils.DAOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class AbstractDao<E> implements GenericDao<E> {

    @PersistenceContext
    protected EntityManager entityManager;

    protected abstract CriteriaQuery<E> getEntityQuery(CriteriaBuilder criteriaBuilder);

    protected abstract Root<E> getEntityRoot(CriteriaQuery<?> query);

    protected abstract Class<E> getEntityClass();

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    public List<E> getAll() throws DAOException {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<E> cq = getEntityQuery(criteriaBuilder);
        Root<E> from = getEntityRoot(cq);
        cq.select(from);
        return getEntityManager().createQuery(cq).getResultList();
    }

    @Override
    public E getById(int id) {
        return getEntityManager().find(getEntityClass(), id);
    }

    @Override
    public E update(E obj) {
        entityManager.contains(obj);
        return getEntityManager().merge(obj);
    }

    @Override
    public E insert(E obj) {
        return getEntityManager().merge(obj);
    }

    @Override
    public void delete(E obj) {
        getEntityManager().remove(obj);
    }

}