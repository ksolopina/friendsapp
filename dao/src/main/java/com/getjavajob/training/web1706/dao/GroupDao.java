package com.getjavajob.training.web1706.dao;

import com.getjavajob.training.web1706.common.Account;
import com.getjavajob.training.web1706.common.Group;
import com.getjavajob.training.web1706.dao.utils.DAOException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.*;
import java.util.List;

@Repository("groupDao")
public class GroupDao extends AbstractDao<Group> {

    public GroupDao() {
    }

    @Override
    protected CriteriaQuery<Group> getEntityQuery(CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.createQuery(Group.class);
    }

    @Override
    protected Root<Group> getEntityRoot(CriteriaQuery<?> criteriaQuery) {
        return criteriaQuery.from(Group.class);
    }

    @Override
    protected Class<Group> getEntityClass() {
        return Group.class;
    }

}
