package com.getjavajob.training.web1706.dao;

import com.getjavajob.training.web1706.common.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("phoneDao")
public class PhoneDao extends AbstractDao<Phone> {

    @Autowired
    private AccountDao accountDao;

    public PhoneDao() {
    }

    @Override
    protected CriteriaQuery<Phone> getEntityQuery(CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.createQuery(Phone.class);
    }

    @Override
    protected Root<Phone> getEntityRoot(CriteriaQuery<?> criteriaQuery) {
        return criteriaQuery.from(Phone.class);
    }

    @Override
    protected Class<Phone> getEntityClass() {
        return Phone.class;
    }

    @Override
    public Phone insert(Phone obj) {
        entityManager.persist(obj);
        entityManager.flush();
        return obj;
    }

    public List<Phone> getAllPhonesByIdAccount(int id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Phone> cq = criteriaBuilder.createQuery(Phone.class);
        Root<Phone> from = cq.from(Phone.class);
        cq.select(from).where(criteriaBuilder.equal(from.get("account"), id));
        return entityManager.createQuery(cq).getResultList();
    }

    public void deleteAllPhonesByIdAccount(int id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete<Phone> cq = criteriaBuilder.createCriteriaDelete(Phone.class);
        Root<Phone> from = cq.from(Phone.class);
        cq.where(criteriaBuilder.equal(from.get("account"), id));
        entityManager.createQuery(cq).executeUpdate();
    }

}
